﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using Subscription.API.Abstractions;
using Subscription.API.Controllers;
using Subscription.API.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Xunit;

namespace SubscriptionApi.Tests
{
    public class AccountControllerTests
    {
        [Fact]
        public async Task Register_ReturnsOK()
        {
            // Arrange
            var mockedService = new Mock<IUserService>();
            mockedService.Setup(service => service.AddAsync(It.IsAny<User>()))
                .ReturnsAsync(1);

            // Act
            var controller = new AccountController(mockedService.Object);
            var result = await controller.Register(new User
            {
                Name = "New user"
            });

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

        [Theory]
        [InlineData(" cannot contain leading space")]
        [InlineData("cannot contain numbers 123")]
        [InlineData("string cannot exceed 64 symbols limit string cannot exceed 64 symbols limit string cannot exceed 64 symbols limit string cannot exceed 64 symbols")]
        public async Task UserModel_Register_GetInvalidModel_WhenNameIsInvalid(string name)
        {
            // Arrange
            var mockedService = new Mock<IUserService>();
            mockedService.Setup(service => service.AddAsync(It.IsAny<User>()))
                .ReturnsAsync(1);
            var user = new User
            {
                Name = name
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var actual = Validator.TryValidateObject(user, new ValidationContext(user), validationResults, true);

            // Assert
            Assert.False(actual);
            Assert.NotEmpty(validationResults);
        }
    }
}
