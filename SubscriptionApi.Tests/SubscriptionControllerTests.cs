using Microsoft.AspNetCore.Mvc;
using Moq;
using Subscription.API.Abstractions;
using Subscription.API.Controllers;
using Subscription.API.Models;
using System.Threading.Tasks;
using Xunit;

namespace SubscriptionApi.Tests
{
    public class SubscriptionControllerTests
    {
        [Fact]
        public async Task Subscribe_ReturnsOK()
        {
            // Arrange
            var mockedService = new Mock<ISubscriptionService>();
            mockedService.Setup(service => service.SubscribeAsync(It.IsAny<long>(), It.IsAny<long>()))
                .ReturnsAsync(SubscribeResult.OK);

            // Act
            var controller = new SubscriptionController(mockedService.Object);
            var result = await controller.Subscribe(1, 2);

            // Assert
            Assert.IsType<OkResult>(result);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async Task Subscribe_ReturnsBadRequest_WhenParamIsZeroOrNegative(int topCount)
        {
            // Arrange
            var mockedService = new Mock<ISubscriptionService>();
            mockedService.Setup(service => service.SubscribeAsync(It.IsAny<long>(), It.IsAny<long>()))
                .ReturnsAsync(SubscribeResult.Error);

            // Act
            var controller = new SubscriptionController(mockedService.Object);
            var result = await controller.TopPopular(topCount);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Subscribe_ReturnsOk_WhenParamIsPositive()
        {
            // Arrange
            var mockedService = new Mock<ISubscriptionService>();
            mockedService.Setup(service => service.SubscribeAsync(It.IsAny<long>(), It.IsAny<long>()))
                .ReturnsAsync(SubscribeResult.Error);

            // Act
            var controller = new SubscriptionController(mockedService.Object);
            var result = await controller.TopPopular(10);

            // Assert
            Assert.IsType<OkObjectResult>(result);
        }

    }
}
