﻿using Microsoft.EntityFrameworkCore;
using Moq;
using Subscription.API.Models;
using Subscription.API.Services;
using Subscription.DB;
using System.Threading.Tasks;
using Xunit;

namespace SubscriptionApi.Tests
{
    public class SubscriptionServiceIntegrationTests
    {
        [Fact]
        public async Task Subscribe_ReturnsError_WhenSubscriberEqualsToRecipient()
        {
            // Arrange
            var mockDbFactory = InitInMemoryDbContextFactory();

            // Act
            var service = new SubscriptionService(mockDbFactory);
            var result = await service.SubscribeAsync(1, 1);

            // Assert
            Assert.Equal(SubscribeResult.Error, result);
        }

        [Fact]
        public async Task Subscribe_ReturnsError_WhenUserDoesNotExist()
        {
            // Arrange
            var mockDbFactory = InitInMemoryDbContextFactory();

            // Act
            var service = new SubscriptionService(mockDbFactory);
            var result = await service.SubscribeAsync(10, 20);

            // Assert
            Assert.Equal(SubscribeResult.Error, result);
        }

        [Fact]
        public async Task Subscribe_ReturnsOk_WhenUsersExist()
        {
            // Arrange
            var mockDbFactory = InitInMemoryDbContextFactory();
            await using var context = mockDbFactory.CreateDbContext();
            await context.Users.AddAsync(new Subscription.DB.Entities.User()
            {
                Id = 10,
                Name = "User Ten"
            });
            await context.Users.AddAsync(new Subscription.DB.Entities.User()
            {
                Id = 20,
                Name = "User Twenty"
            });
            await context.SaveChangesAsync();

            // Act
            var service = new SubscriptionService(mockDbFactory);
            var result = await service.SubscribeAsync(10, 20);

            // Assert
            Assert.Equal(SubscribeResult.OK, result);
        }

        private IDbContextFactory<SubscriptionDbContext> InitInMemoryDbContextFactory()
        {
            var mockDbFactory = new Mock<IDbContextFactory<SubscriptionDbContext>>();
            mockDbFactory.Setup(f => f.CreateDbContext())
                .Returns(() => new SubscriptionDbContext(new DbContextOptionsBuilder<SubscriptionDbContext>()
                    .UseInMemoryDatabase("InMemoryTest")
                    .Options));
            return mockDbFactory.Object;
        }
    }
}
