﻿using Microsoft.EntityFrameworkCore;
using Subscription.DB.Entities;

namespace Subscription.DB
{
    public class SubscriptionDbContext : DbContext
    {
        public SubscriptionDbContext(DbContextOptions<SubscriptionDbContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Entities.Subscription> Subscriptions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasIndex(u => u.Name)
                .IsUnique();

            modelBuilder.Entity<Entities.Subscription>()
                .HasIndex($"{nameof(Entities.Subscription.Subscriber)}Id", $"{nameof(Entities.Subscription.Recipient)}Id")
                .IsUnique();
        }
    }
}