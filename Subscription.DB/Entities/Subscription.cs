﻿using System.ComponentModel.DataAnnotations;

namespace Subscription.DB.Entities
{
    public class Subscription
    {
        public long Id { get; set; }

        [Required]
        public User Subscriber { get; set; }

        [Required]
        public User Recipient { get; set; }
    }
}
