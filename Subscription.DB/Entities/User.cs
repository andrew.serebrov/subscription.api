﻿using System.ComponentModel.DataAnnotations;

namespace Subscription.DB.Entities
{
    public class User
    {
        public long Id { get; set; }

        [Required]
        [MaxLength(64)]
        public string Name { get; set; }
    }
}