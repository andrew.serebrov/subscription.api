﻿using Microsoft.AspNetCore.Mvc;
using Subscription.API.Abstractions;
using Subscription.API.Models;
using System.Threading.Tasks;

namespace Subscription.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Registers a new user and returns it's identifier.
        /// </summary>
        /// <param name="user">User to create.</param>
        /// <returns>Identifier of the newly created user.</returns>
        [HttpPost]
        public async Task<IActionResult> Register(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrors());
            }
            var id = await _userService.AddAsync(user);
            return Ok(id);
        }
    }
}