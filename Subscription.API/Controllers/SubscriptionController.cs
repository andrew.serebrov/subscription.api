﻿using Microsoft.AspNetCore.Mvc;
using Subscription.API.Abstractions;
using System.Threading.Tasks;

namespace Subscription.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SubscriptionController : ControllerBase
    {
        private readonly ISubscriptionService _subscriptionService;

        public SubscriptionController(ISubscriptionService subscriptionService)
        {
            _subscriptionService = subscriptionService;
        }

        /// <summary>
        /// Subscribes one user to another.
        /// </summary>
        /// <param name="subscriberId">Identifier of the user who wants to subscribe.</param>
        /// <param name="recipientId">Identifier of the user to subscribe to.</param>
        [HttpPost]
        [Route("Subscribe")]
        public async Task<IActionResult> Subscribe(long subscriberId, long recipientId)
        {
            await _subscriptionService.SubscribeAsync(subscriberId, recipientId);

            return Ok();
        }

        /// <summary>
        /// Returns the list of most popular users.
        /// </summary>
        /// <param name="topCount">The amount of records to return.</param>
        /// <returns>List of most popular users sorted by popularity, most popular first.</returns>
        [HttpGet]
        [Route("Top")]
        public async Task<IActionResult> TopPopular(int topCount)
        {
            if (topCount <= 0)
                return BadRequest("'topCount' parameter cannot be zero or negative.");

            var topUsers = await _subscriptionService.GetTopPopularAsync(topCount);

            return Ok(topUsers);
        }
    }
}
