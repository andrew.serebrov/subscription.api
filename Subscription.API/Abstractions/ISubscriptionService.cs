﻿using Subscription.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Subscription.API.Abstractions
{
    public interface ISubscriptionService
    {
        public Task<SubscribeResult> SubscribeAsync(long subscriberId, long recipientId);
        public Task<IReadOnlyList<UserTopResult>> GetTopPopularAsync(int amount);
    }
}