﻿using Subscription.API.Models;
using System.Threading.Tasks;

namespace Subscription.API.Abstractions
{
    public interface IUserService
    {
        public Task<long> AddAsync(User user);
    }
}