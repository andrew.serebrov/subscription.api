﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;

namespace Subscription.API
{
    public static class ModelStateExtensions
    {
        public static string GetErrors(this ModelStateDictionary ModelState)
        {
            return string.Join("; ", ModelState.Values
                .SelectMany(x => x.Errors)
                .Select(x => x.ErrorMessage));
        }
    }
}