﻿using Microsoft.EntityFrameworkCore;
using Subscription.API.Abstractions;
using Subscription.API.Models;
using Subscription.DB;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Subscription.API.Services
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IDbContextFactory<SubscriptionDbContext> _contextFactory;

        public SubscriptionService(IDbContextFactory<SubscriptionDbContext> contextFactory)
        {
            this._contextFactory = contextFactory;
        }

        public async Task<IReadOnlyList<UserTopResult>> GetTopPopularAsync(int amount = 5)
        {
            await using var context = _contextFactory.CreateDbContext();

            var topIds = (from s in context.Subscriptions
                          group s by s.Recipient.Id into g
                          orderby g.Key descending
                          select g.Key).Take(amount);

            var topUsers = from i in topIds
                           join u in context.Users on i equals u.Id
                           select u;

            var result = await topUsers.Select(u => new UserTopResult()
            {
                Id = u.Id,
                Name = u.Name
            }).ToListAsync();

            return result;
        }

        public async Task<SubscribeResult> SubscribeAsync(long subscriberId, long recipientId)
        {
            if (subscriberId == recipientId)
                return SubscribeResult.Error;

            await using var context = _contextFactory.CreateDbContext();
            var subscriber = await context.Users.FindAsync(subscriberId);
            if (subscriber == null)
                return SubscribeResult.Error;

            var recipient = await context.Users.FindAsync(recipientId);
            if (recipient == null)
                return SubscribeResult.Error;

            var existingSubscriptions = await context.Subscriptions.AnyAsync(s => s.Recipient.Id == recipientId && s.Subscriber.Id == subscriberId);
            if (existingSubscriptions)
                return SubscribeResult.Error;

            await context.Subscriptions.AddAsync(new DB.Entities.Subscription
            {
                Recipient = recipient,
                Subscriber = subscriber
            });

            await context.SaveChangesAsync();
            return SubscribeResult.OK;
        }
    }
}