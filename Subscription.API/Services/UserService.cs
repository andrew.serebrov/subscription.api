﻿using Microsoft.EntityFrameworkCore;
using Subscription.API.Abstractions;
using Subscription.API.Models;
using Subscription.DB;
using System.Threading.Tasks;

namespace Subscription.API.Services
{
    public class UserService : IUserService
    {
        private readonly IDbContextFactory<SubscriptionDbContext> contextFactory;

        public UserService(IDbContextFactory<SubscriptionDbContext> contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public async Task<long> AddAsync(User user)
        {
            await using var context = contextFactory.CreateDbContext();
            var entity = new DB.Entities.User
            {
                Name = user.Name
            };
            await context.AddAsync(entity);
            await context.SaveChangesAsync();
            return entity.Id;
        }
    }
}
