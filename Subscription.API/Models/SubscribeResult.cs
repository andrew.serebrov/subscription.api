﻿namespace Subscription.API.Models
{
    public enum SubscribeResult
    {
        Error = -1,
        OK = 1,
    }
}