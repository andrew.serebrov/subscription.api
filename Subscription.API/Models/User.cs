﻿using System.ComponentModel.DataAnnotations;

namespace Subscription.API.Models
{
    /// <summary>
    /// User.
    /// </summary>
    public class User
    {
        /// <summary>
        /// The name of the user.
        /// </summary>
        [Required]
        [RegularExpression(
            pattern: @"^[a-zA-Z]+[a-zA-Z ]*$",
            ErrorMessage = "Only letters and spaces are allowed. Leading spaces are not allowed.")]
        [StringLength(64)]
        public string Name { get; set; }
    }
}