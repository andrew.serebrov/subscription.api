﻿namespace Subscription.API.Models
{
    public class UserTopResult
    {
        /// <summary>
        /// Identifier of the user. 
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// The name of the user.
        /// </summary>
        public string Name { get; set; }
    }
}