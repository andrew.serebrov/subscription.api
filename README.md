## Subscription API

#### Сборка и запуск

Открыть .sln в VS 2019, собрать и запустить проект Subscription.API с настройками по умолчанию.  
*Трeбуется одна из последних версий VS 2019 с поддержкой .NET 5.*  

#### Использование

1. При запуске приложение открывает страницу документации swagger, которая содержит подробные описания методов и позволяет вызывать методы контроллеров.  
2. Простейшая последовательность действий:  
   1. Добавление пользователей.  
      ```
      curl -X POST "https://localhost:5001/Account" -H  "accept: */*" -H  "Content-Type: application/json" -d "{\"name\":\"First User\"}"
      curl -X POST "https://localhost:5001/Account" -H  "accept: */*" -H  "Content-Type: application/json" -d "{\"name\":\"Second User\"}"
      ```  
   2. Добавление подписки одного пользователя на другого.  
      ```
      curl -X POST "https://localhost:5001/Subscription/Subscribe?subscriberId=1&recipientId=2" -H  "accept: */*" -d ""
      ```
   3. Получение списка пользователей, отсортированных по убыванию популярности (количества подписчиков).  
      ```
      curl -X GET "https://localhost:5001/Subscription/Top?topCount=5" -H  "accept: */*"
      ```

#### Допущения и ограничения

1. В проекте использованы стандартные концепции ASP.NET, EF.Core и библиотеки: Контроллеры, EF.Core migrations, XUnit тесты, моки c использованием библиотеки Moq.  
   Это позволило максимально сэкономить время на разработку.
2. В целях ускорения разработки приняты следующие упрощения:  
   1. Разделение на слои абстракций/уровни остветственности произведено условно. 
      Присутствует отдельный проект для работы с БД (`Subscription.DB`). Вся остальная логика находится в стартовом проекте (`Subscription.API`).
   2. Заведены несколько примеров юнит и интеграционных тестов, демонстрирующих принципиальные возможности покрытия тестами. Задача обеспечения полного покрытия тестами не ставилась.  
   3. Метод получения списка наиболее популярых пользователей не рассчитан на большие объемы данных, в этом случае вероятны проблемы с производительностью.
   4. Методы и классы имеют минимум проверок на входные параметры и результаты.
   5. Настройки не реализовывались. Минимальные настройки прописаны в коде (connection string).